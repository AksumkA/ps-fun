## USMT PS Script

### Usage
- Edit paths to scan/loadstate.exes, profile save location, and Mig.xml files
- Assumes 64-bit OS. Make sure to change paths if still on 32-bit
- Run from an elevated PS prompt
- Will automatically populate current user
- Prompts will walk you though the options
  - (S)can - Copy profile
  - (L)oad - Restore profile
- Loading will auto populate last modified folder from profile path
- Logging is enabled, will dump prog.log and scan.log 

If not signed, you can use a oneliner to run the script:

```
powershell -executionpolicy bypass -file "\\PATH_TO\scanstate.ps1"
```