if (!([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")) { Start-Process powershell.exe "-NoProfile -ExecutionPolicy Bypass -File `"$PSCommandPath`"" -Verb RunAs; exit }

cls

# EXEs for scan/loadstate
$scanstate="\\PATH_TO\USMT\amd64\scanstate.exe";
$loadstate="\\PATH_TO\USMT\amd64\loadstate.exe";

# Path to user profiles
$netProfilePath="\\PATH_TO\USMT\users\";
$localProfilePath="C:\USMT\";

# Config files
$migUserConf="\\PATH_TO\USMT\amd64\MigUser.xml";
$migAppConf="\\PATH_TO\USMT\amd64\MigApp.xml";
$OTHERConf="\\PATH_TO\USMT\amd64\OTHER.xml";

Write-Host 'USMT' -Foregroundcolor Green;
Write-Host '-> This tool will migrate documents and some application settings!' -Foregroundcolor Green;

# Function to display a nicer looking error message
# $msg <string>
function errorMsg($msg) {
    $msg = "  " + $msg + "  ";
    Write-Host " Error! " -Foregroundcolor Black -Backgroundcolor Red;
    Write-Host $(' ' * $msg.length) -Foregroundcolor Black -Backgroundcolor Black;
    Write-Host $msg -Foregroundcolor Red -Backgroundcolor Black;
    Write-Host $(' ' * $msg.length) -Foregroundcolor Black -Backgroundcolor Black;
    echo '';
}

Do {
    $success = $true;
    # Are we going to scan or load?
    Write-Host "(S)can" -ForegroundColor Yellow -NoNewline
    Write-Host " or " -NoNewline
    Write-Host "(L)oad" -ForegroundColor Green -NoNewline
    Write-Host " state? " -NoNewline
    $state = Read-Host

    # Where should we dump the user profile?
    $action = if ($state -eq 's') {'Save on '} else {'Load from '}
    Write-Host $action -NoNewline
    Write-Host "[N]etwork" -ForegroundColor Yellow -NoNewline
    Write-Host " or " -NoNewline
    Write-Host "(C)ustom? " -ForegroundColor Green -NoNewline
    $location = Read-Host

    if($location -eq 'c') {
        Write-Host "Location " -NoNewline
        Write-Host "[C:\usmt\]" -ForegroundColor Green -NoNewline
        Write-Host ": " -NoNewline

        if(($customLocation = Read-Host) -eq '') {
            # Use default local path
            $profilePath = $localProfilePath
        } elseif (!(Test-Path -Path ($customLocation) )) {
                errorMsg("Selected folder does not exist");
                $success = $false;
                break
        } else {
            # Append a trailing \ if missing
            if ($customLocation -notmatch '.+?\\$') {
                $customLocation += '\'
            }
            # Custom path all good
            $profilePath = $customLocation;
        }

    } else {
        # Use default network path
        $profilePath = $netProfilePath;
    }

    Write-Host "Profile location: " -NoNewline
    Write-Host  $profilePath -ForegroundColor Green

    $StopWatch = New-Object System.Diagnostics.Stopwatch
    $StopWatch.Start()

    if($state -eq 's') {
        # Ask for username (Default to current logged in user, stip out domain)
        $currentUser=$(Get-WMIObject -class Win32_ComputerSystem | select username).username.split('\')[1]

        Write-Host "Username to migrate " -NoNewline
        Write-Host "[$currentUser]" -ForegroundColor Yellow -NoNewline
        Write-Host ": " -NoNewline

        if(($targetUser = Read-Host) -eq '') {
            $targetUser=$currentUser
        }

        # Check that this folder exists
        if(!(Test-Path -Path ("C:\Users\" + $targetUser) )){
            errorMsg("User not found");
            $success = $false;
        }

        # Scanstate Starting
        if ($success) {

            echo '';
            Write-Host "--> " -ForegroundColor Yellow -NoNewline
            Write-Host "ScanState Started! Please Wait..." -ForegroundColor Green -NoNewline
            Write-Host " <--" -ForegroundColor Yellow -NoNewline
            echo '';

            $userPath = ($profilePath + $targetUser);

            # Run the actual EXE
            Start-Process -FilePath $scanstate `
                -ArgumentList "$userPath /ue:* /ui:$targetUser /i:$migUserConf /i:$migAppConf /i:$OTHERConf /progress:`"$userPath\prog.log`" /v:4 /l:`"$userPath\scan.log`"" `
                -Wait `
                -NoNewWindow
        }

    } elseif ($state -eq 'l') {
        # Assume the last modified folder is our target 
        $latestFolder = Get-ChildItem -Path $profilePath | Where-Object {$_.PSIsContainer} | # limit search to folders only
            Sort-Object LastWriteTime -Descending | 
            Select-Object -First 1;

        Write-Host "Username to migrate " -NoNewline
        Write-Host "[$latestFolder]" -ForegroundColor Green -NoNewline
        Write-Host ": " -NoNewline

        if(($targetUser = Read-Host) -eq '') {
            $targetUser=$latestFolder
        }

        # Check that this folder exists
        if(!(Test-Path -Path ($profilePath + $targetUser) )){
            errorMsg("Folder not found");
            $success = $false;
        }

        # Loadstate Starting
        if ($success) {
            echo '';
            Write-Host "--> " -ForegroundColor Yellow -NoNewline
            Write-Host "LoadState Started! Please Wait..." -ForegroundColor Green -NoNewline
            Write-Host " <--" -ForegroundColor Yellow -NoNewline
            echo '';

            $userPath = ($profilePath + "\" + $targetUser);

            # Run the actual EXE
            Start-Process -FilePath $loadstate `
                -ArgumentList "$userPath /ue:* /ui:$targetUser /i:$migUserConf /i:$migAppConf /i:$OTHERConf /c /progress:`"$userPath\prog.log`" /v:4 /l:`"$userPath\scan.log`"" `
                -Wait `
                -NoNewWindow
        }

    } else {
        errorMsg("Should have pressed either S or L");
        $success = $false;
    }

    if ($success) {
        echo '';
        Write-Host "--> " -ForegroundColor Yellow -NoNewline
        Write-Host "$targetUser has been migrated!" -ForegroundColor Green -NoNewline
        Write-Host " <--" -ForegroundColor Yellow -NoNewline
        echo '';

        $hour = $StopWatch.Elapsed.hours
        $min =  $StopWatch.Elapsed.minutes
        $sec =  $StopWatch.Elapsed.seconds

        "Took: {0:00}:{1:00}:{2:00}" -f $hour, $min, $sec
    }
}
While (!$success)
